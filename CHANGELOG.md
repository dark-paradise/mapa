# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- p�id�n� nov�ch barvi�ek
- p�id�n� klienta 2.0.0
- p�id�n� fallback klienta 1.26.4b

### Changed
- zm�na ip v login.cfg (z IP na dom�nu)
- dopln�n� nov�ho version stringu (2.6) do prav�ho doln�ho rohu mapy
- mapa obsahuje pouze zm�n�n� soubory, zbytek soubor� mus� b�t z klienta 7.0.23.1 (ke sta�en� na webu)

### Removed

- odstran�n� custom DP credits (ji� d�vno v�e jinak)
- odebr�n� custom povol�n� z tvorby postavy (nastaveno na p�vodn� v�ci z klienta)
- d��ve custom soubory (anim.idx, anim.mul, animdata.mul, art.def, art.mul, artidx.mul, Credits.dat, gumpart.mul, gumpart.idx, options.enu, prof.txt, Professn.enu, radacol.mul, skillgrp.mul, Skills.idx, skills.mul, texidx.mul, texmaps.mul) jsou nyn� dle origin�ln�ho klienta

## [2.5.6] - 2014-03-23

### Added

- stany pro hr��sk� vendory

## [2.5.5] - 2014-03-23

### Changed

- ???

## [2.5.0] - 2013-02-22

### Added

- p�i tvorb� postavy p�id�na mo�nost HRDINA pou�iteln� pro rychlej�� vytvo�en� postav
- v�echny dekorace vendor domk� p�id�ny do mapy
- p�id�ny nov� domky do multis

### Fixed

- opraven tajn� vchod k lsn v Terathan Keep
- oprava vchodu k lam� ve Scourge Citadele
- oprava zdi ve fire m�stnosti ve Fan Dancer Dojo
- oprava lodi ve Vendett�
- oprava vody (3489,580)(2917,2041)
- oprava zapad�v�n� na (465,245)(3602,1787)
- oprava Trinsic -> Delucia pr�chodu
- opravena Log Cabin

### Changed

- srovn�na zem� kolem dve�� (5167,3181)
- zm�na Deceitu (5307,675), aby ne�lo portit vedle
- upraven schod v ter�nu (5937,4013)
- srovn�n ter�n v Ice Dungeonu
- v�echny n�jemn� domy maj� pouze jeden vchod
- upraveny Credits

### Removed

- odebr�ny n�kter� mo�nosti p�i p�ihla�ov�n� do UO

## [2.4.1] - 2011-10-07

### Added

- hythloth pridan do 
- p�id�no molo v Umb�e (3661,516)
- p�id�n kraken minidung + cyclop minidung kousek vedle
- p�id�na drow camp v T2A (5347,3813)
- p�id�n bar�k Pet Leash tradera
- p�id�na hranice (5300,3400)
- p�id�n kemp kyklop� (5267,3356)
- p�id�ny pevn�stky (1134,2600)(776,1648)
- p�id�na pevnost v Occlu (3312,1992)
- p�id�n p��stav (2344,2500)
- p�id�na mini ar�na (6080,3720)
- p�id�n zmrzl� les (5149,2366)
- p�id�n orc minidung T2A (5168,3200)
- p�id�na Alej mrtv�ch strom� (6113,3864)
- p�id�n h�bitov zapomenut�ho �asu (6069,3787)
- p�id�na Britainsk� kolonie (5406,3438)
- p�id�na voda kolem S�n� mystik�
- p�id�na Scourge Citadel
- p�id�n Deceit
- p�id�n Fan Dancer Dojo
- p�id�n Wisp
- p�id�n quest Po stop�ch ryt��e
- p�id�n Sorcerer's dungeon
- p�id�no TK
- p�id�ny Ratman Mines
- p�id�na Vendetta
- p�id�n Vamp klan quest
- p�id�na Forbidden Library
- p�id�n Ankh
- p�id�n Heroic Temple
- p�id�n quest na elvenku a war nahrdelnik
- postaveno Elemental Planes
- zkop�rov�no posledn� patro Bloodu
- p�id�n konec yomotsu mines

### Fixed

- oprava gm svatebn� s�n�
- oprava vody (4519,514)(1402,436)(4581,591)(5784,3037)(5650,2367)
- oprava d�ry (5671,3276)(5807,3473)(5344,3389)
- oprava mostu v serpents hold
- oprava cest v T2A (5412,3772)(5510,3676)(5679,2662)
- oprava pyramidy v Lun�
- oprava schod� v Lun�
- oprava podlahy v Happy Arthrosis Tavern
- oprava podlahy v s�ni mystik�
- oprava zapad�v�n� kolem (5757,2579)(5481,3490)
- oprava domku v Umb�e proti vykraden�
- oprava proti zapad�v�n� (3636,465)(5551,2626)
- oprava vody v T2A (5363,3380)(5363,3446)
- oprava sk�ly v T2A (5636,3298)
- oprava cesty (5545,3614)(5720,2674)
- oprava ter�nu na (5911,3276)
- oprava vody kolem alternativn�ho vchodu do Fanatics Cave
- oprava zapad�v�n� u jez�rka v Delucii
- opraveno propad�v�n� ze Sandstone Patio House
- oprava trade okna

### Changed

- �adov� domky v Lun� zv�t�eny
- �prava hradeb v Lun� proti vykr�d�n� + p�id�ny zdi v domc�ch do statiku
- zm�na vchodu do stok v Britu
- �prava Trap! (5400,3787)
- �prava vody kolem Serpents Hold
- �prava domku 21,62,105,109,238,252,110xx
- upraven Exodus
- upraven vodop�d (587,1625)
- upraven konec Citadely
- �prava m�stnosti v Ankhu
- ohrani�eno TK (kolem 5466,3088)
- upraven Large Marble Patio House
- upraven Sanstone Patio House

### Removed

- odstran�n nesmysln� uo.reg soubor v archivu mapy

## [2.4.0] - 2011-10-04

### Changed

- ???

## [2.3.6] - 2010-10-29

### Added

- 4 nove karibiky
- Valley of Survivors
- pridana svatebni sin do mapy
- pridano hriste clovece nezlob se do mapy
- nova tiledata (uprava stolu, nestockovatelne kegy, opravene futton)
- nove barvicky
- nove texmapy a arty
- cech krys
- cech nekru
- zkopirovan fire caern a dark panther lair
- zkopirovano fdd 3, yomotsu 1 a 2
- pridana Umbra
- pridano molo pred pride
- pridano molo pred fanatikama
- pridana Luna
- cesty k Umbre a Lune
- hotel kolem 2800,500
- pridany animace nekterych itemu (zbrane/zbroje)
- pridany animace novych npc

### Changed

- posunuto ice
- 2 male pruchody posunuty
- posunut wrong konec
- posunut nb dung a nb barak
- posunuta torture room v cove
- posunut konec cove
- posunuty schody v sorcu
- posunuty qp 17
- posunuta shadow spider lair
- posunuto daemons brothers
- posunuta daemons cave
- posunuty qp 04
- posunuta ziva voda
- posunut vamp klan quest stary
- zmena cesty u jailu
- premisten pruchod del-trin-nb runa
- v nb dungu v prvni mistnosti lze kopat
- zmenena krizovatka v despu
- presunuta arena v trinu (domination)
- upraveny schody v hytlu 5920,170 a ty posledni
- upravena banka v caernu
- obezden exodus
- pri tvorbe postavy se uz neobjevuji jine moznosti nez advanced
- upraven strom na 4702,2270

### Fixed

- snad oprava skills tabulky, komu nefunguje at si smaze v desktopech skillgrp tabulku
- nelze krast pres okna
- oprava trade okna
- oprava drobnych chyb v grafice terenu
- opravena voda na: 2293,110; 2656,1790; 2551,0; 3285,88; 4425,342; 3326,1952; 789,509; 4749,222; 4872,272; 1419,373; 3016,1119
- oprava schodu na vamp klan questu
- opraveny v podzemi pod buxem

### Removed

- smazan malas
- odstraneny statiky v baraccich

## [2.3.1] - 2010-08-12

### Changed

- ???

## [2.3.0] - 2010-08-10

### Changed

- ???

## [2.2.1] - 2009-03-16

### Added

- pridan dungeon prism of light
- pridan dungeon sanctuary
- pridan dungeon palace of paroxysmus 
- pridan dungeon blighted grove
- pridan dungeon painted caves
- pridan dungeon twisted weald
- pridan dungeon labyrinth
- pridan dungeon bedlam
- pridan dungeon citadel

### Changed

- predelana papua
- upravena krizovatka v despu
- upraveno fire
- upraven doom
- upraven konec terathanu
- upraveno okoli jesli
- uprava newbie hostince
- upravena plaz v nujelmu
- upraveno par domku
- srovnano terra sanctum a cyclop valley

### Fixed

- opraveny vezicky v drow city 5906x1494
- opravena reka v drow city
- oprava zobrazovani skillu

## [2.1.0] - 2007-12-04

### Added

- nove multis
- zdi v doomu
- opet pridana zed v shame
- pridano solen hive

### Changed

- obezdeno 5750,345
- obezdeno 5830,445
- obezdeno 5680,405
- obezdeno 5735,440
- upravena cesta kolem jailu
- upraveno 5694,2523 - schudnejsi
- upraven vchod do ts
- upravena tiledata - stoly, kytky

### Fixed

- opraveno 5560,2526
- opraveno 6083,1484 - zapadavaci policko
- opraveno probihani mobu na dne ice 5830,350
- opravene domky - 5333,1608; 2072,2334; 5938,1417; 5978,1498;5494,3224

### Removed

- u skillu Stealh odebrano spousteci tlacitko
- smazan kus malasu na 4810,2380

## [2.0.0] - 2006-07-12

### Added

- postaven daemon dung
- dalsi kus zdi v shame
- prehrada v ilshi
- instalator 1.0
- supertajny misto

### Changed

- prekopano ts, zazdeny mista na zasekavani mobu

[Unreleased]: https://gitlab.com/dark-paradise/mapa/-/compare/v2.5.6...master
[2.5.6]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.5.6
[2.5.5]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.5.5
[2.5.0]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.5.0
[2.4.1]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.4.1
[2.3.6]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.3.6
[2.2.1]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.2.1
[2.1.0]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.1.0
[2.0.0]: https://gitlab.com/dark-paradise/mapa/-/releases/v2.0.0

